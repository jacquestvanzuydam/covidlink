import "./styles.scss";

var sacoronavirus =(function () {
  'use strict'

  const linkAddress = "www.sacoronavirus.co.za";
  var methods = {};
  function container(text, className) {
    const div = document.createElement('DIV');
    div.innerText = `${text}: `;
    div.className = className;
    return div;
  }
  // default options {target: "", text: "" , theme: "light", addClass: ""}
  methods.createLink = function(options) {
    // set the defaults
    if (!options.target) {
      options.target = document.body;
    }
    if (!options.text) {
      options.text = "For more information about the COVID-19 pandemic in South Africa";
    }
    if (!options.theme) {
      options.theme = 'light';
    }
    if (!options.addClass) {
      options.addClass = '';
    }
    const link = document.createElement('A');
    link.href=`http://${linkAddress}`;
    link.innerText = linkAddress;
    link.target = "_blank";
    link.rel = "noopener";
    const div = container(options.text, `covid-link ${['light', 'dark'].indexOf(options.theme) >= 0 ? options.theme : 'light'} ${options.addClass}`)
    div.appendChild(link);
    options.target.prepend(div)

  }

  return methods;
})();

window.sacoronavirus = sacoronavirus;
