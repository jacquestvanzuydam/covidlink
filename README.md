# covidlink

This is a simple script that builds a vanilla JS module to insert the `sacoronavirus` link into any website by including the script and calling the method:

```
  sacoronavirus.createLink();
```

## Building and running on localhost

First install dependencies:

```sh
npm install
```

To create a production build:

```sh
npm run build-prod
```

To create a development build:

```sh
npm run build-dev
```

## Running

```sh
node dist/bundle.js
```

## Credits

Made with [createapp.dev](https://createapp.dev/)
